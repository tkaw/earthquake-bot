from datetime import datetime
import requests

url = "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&latitude=36.7783&longitude=119.4179&maxradiuskm=1250"
data = requests.get(url).json()

earthquakes = data['features']
file_name = 'earthquake data.rtf'

with open(file_name, 'w') as file_object:
	for earthquake in earthquakes:
		place_first = ""
		place_last = ""
		count = 0

		for c in place:
			if (count_1 <= int(len(earthquake['properties']['place']) / 2) + 1):
				place_first += c
			else:
				place_last += c
			count += 1

		timestamp = int(str(earthquake['properties']['time'])[:-3])
		eq_date = datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
		
		file_object.write(place_first + "\n")
		file_object.write(place_last + "\n")
		file_object.write(eq_date + '\n')
		file_object.write(str(earthquake['properties']['mag']) + '\n')