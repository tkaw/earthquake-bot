from datetime import datetime
import requests

url = "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&latitude=36.7783&longitude=119.4179&maxradiuskm=1250"
response = requests.get(url)
data = response.json()
earthquakes = data['features']

file_name = 'earthquake data.rtf'

with open(file_name, 'w') as file_object:
	for earthquake in earthquakes:
		file_object.write(earthquake['properties']['place'] + "\n")
		timestamp = int(str(earthquake['properties']['time'])[:-3])
		eq_date = datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
		file_object.write(eq_date + '\n')
		file_object.write(str(earthquake['properties']['mag']) + '\n')

# print(earthquakes)